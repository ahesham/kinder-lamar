import os, sys
from subprocess import check_call, Popen, PIPE, STDOUT

def setup(work_dir):
	venv_dir = os.path.join(work_dir, 'env')
	if os.path.isdir(venv_dir):
		return

	# Create a virtual environment and use the Python interpreter provided by it
	check_call([sys.executable, '-m', 'venv', venv_dir])
	if os.name == 'nt':
		python = os.path.join(venv_dir, 'Scripts', 'python.exe')
	else:
		python = os.path.join(venv_dir, 'bin', 'python')

	# Install the required Python packages
	check_call([python, '-m', 'pip', 'install', 'sphinx', '-q', '--disable-pip-version-check', '--no-cache-dir'])
	check_call([python, '-m', 'pip', 'install', 'breathe', '-q', '--disable-pip-version-check', '--no-cache-dir'])

def build(doxygen, work_dir, include_dir, output_dir):
	doc_dir = os.path.dirname(os.path.realpath(__file__))
	xml_dir = os.path.join(work_dir, 'xml')

	# Use Doxygen to extract API documentation from headers in XML form
	cmd = [doxygen, '-']
	pipe = Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=STDOUT)
	output, _ = pipe.communicate(input = fr'''
		PROJECT_NAME        = core
		GENERATE_LATEX      = NO
		GENERATE_MAN        = NO
		GENERATE_RTF        = NO
		GENERATE_HTML       = NO
		GENERATE_XML        = YES
		XML_OUTPUT          = {xml_dir}
		INPUT               = {include_dir}
		RECURSIVE           = YES
		CASE_SENSE_NAMES    = NO
		QUIET               = YES
		JAVADOC_AUTOBRIEF   = YES
		AUTOLINK_SUPPORT    = NO
		ALIASES             = "rst=\verbatim embed:rest"
		ALIASES            += "endrst=\endverbatim"
		MACRO_EXPANSION     = YES
		'''.encode('UTF-8'))
	output = output.decode('utf-8')
	print(output)

	if pipe.returncode != 0:
		raise CalledProcessError(pipe.returncode, cmd)

	# Use Sphinx to generate the HTML documentation
	html_dir = os.path.join(output_dir, 'html')
	if os.name == 'nt':
		sphinx_path = os.path.join(work_dir, 'env', 'Scripts', 'sphinx-build.exe')
	else:
		sphinx_path = os.path.join(work_dir, 'env', 'bin', 'sphinx-build')
	check_call([sphinx_path, '-Dbreathe_projects.core=' + os.path.abspath(xml_dir),
				'-b', 'html', doc_dir, html_dir])

	return html_dir

if __name__ == '__main__':
	doxygen = sys.argv[1]
	work_dir = sys.argv[2]
	include_dir = sys.argv[3]
	output_dir = sys.argv[4]

	setup(work_dir)
	build(doxygen, work_dir, include_dir, output_dir)
