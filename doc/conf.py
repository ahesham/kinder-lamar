extensions = ['breathe']

breathe_default_project = 'core'
breathe_domain_by_extension = { 'h' : 'cpp' }

source_suffix = '.rst'

project = 'core'

copyright = '2022-present, Ahmed Hesham'

exclude_patterns = ['vritualenv']

default_role = 'cpp:any'

pygments_style = 'sphinx'

highlight_language = 'c++'

primary_domain = 'cpp'
