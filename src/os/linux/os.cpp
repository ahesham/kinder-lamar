#include "kl/core/os.h"

#include <assert.h>

#include <unistd.h>
#include <string.h>
#include <linux/limits.h>

namespace
{
    template<typename T, size_t N>
    constexpr size_t countof(const T(&)[N]) noexcept
    {
        return N;
    }
}

namespace kl::os
{
    std::string
    current_dir() noexcept
    {
    	char buffer[PATH_MAX] = { 0 };
    	const char* result = ::getcwd(buffer, countof(buffer));
    	assert(result != nullptr);

    	return std::string(buffer, ::strlen(buffer));
    }

    std::string
    current_exe() noexcept
    {
    	char buffer[PATH_MAX] = { 0 };
    	const ssize_t result = ::readlink("/proc/self/exe", buffer, countof(buffer));
    	assert(result != -1);

    	return std::string(buffer, ::strlen(buffer));
    }
}
