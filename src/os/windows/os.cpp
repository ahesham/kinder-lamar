#include "kl/core/os.h"

#include <stdint.h>
#include <assert.h>
#include <string_view>

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

namespace
{
    template<typename T, size_t N>
    constexpr size_t countof(const T(&)[N]) noexcept
    {
        return N;
    }

    static inline std::string
    ToUTF8(std::wstring_view input)
    {
        const int size = ::WideCharToMultiByte(CP_UTF8, 0, input.data(), -1, 0, 0, 0, 0);
        std::string result(size, 0);
        ::WideCharToMultiByte(CP_UTF8, 0, input.data(), -1, result.data(), size, 0, 0);

        return result;
    }

    static inline std::wstring
    ToUTF16(std::string_view input)
    {
        const int size = ::MultiByteToWideChar(CP_UTF8, 0, input.data(), -1, 0, 0);
        std::wstring result(size, 0);
        ::MultiByteToWideChar(CP_UTF8, 0, input.data(), -1, result.data(), size);

        return result;
    }
}

namespace kl::os
{
    std::string
    current_dir() noexcept
    {
        wchar_t buffer[1024] = { 0 };

        const DWORD result = ::GetCurrentDirectoryW((DWORD)countof(buffer), buffer);
        if (result >= countof(buffer))
        {
            wchar_t* dynamic_buffer = new wchar_t[result];
            ::GetCurrentDirectoryW(result, dynamic_buffer);
            const std::string dir = ToUTF8(dynamic_buffer);
            delete[] dynamic_buffer;
            return dir;
        }

        return ToUTF8(buffer);
    }

    std::string
    current_exe() noexcept
    {
        wchar_t buffer[1024] = { 0 };

        const DWORD result = ::GetModuleFileNameW(0, buffer, (DWORD)countof(buffer));
        assert(result < countof(buffer));

        return ToUTF8(buffer);
    }
}
