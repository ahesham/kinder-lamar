#pragma once

#include <string>
#include <string_view>

namespace kl::path
{
    std::string
    base(std::string_view path) noexcept;

    std::string
    clean(std::string_view path) noexcept;

    std::string
    dir(std::string_view path) noexcept;

    std::string
    ext(std::string_view path) noexcept;

    bool
    is_abs(std::string_view path) noexcept;
}
