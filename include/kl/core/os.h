#pragma once

#include <string>

namespace kl::os
{
    /**
     * Get the current working directory.
    */
    std::string
    current_dir() noexcept;

    /*!
    * Get the current executable path.
    */
    std::string
    current_exe() noexcept;
}
