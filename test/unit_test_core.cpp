#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"

#include "kl/core/os.h"

TEST_CASE("Test the OS module")
{
    SUBCASE("kl::os::current_dir")
    {
        const std::string cd = kl::os::current_dir();
        CHECK(!cd.empty());
    }

    SUBCASE("kl::os::current_exe")
    {
        const std::string executable = kl::os::current_exe();
        CHECK(!executable.empty());
    }
}

TEST_CASE("Test the Path module")
{

}
